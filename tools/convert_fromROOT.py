import sys
import uproot as up
import numpy as np
import h5py
import pandas as pd
from Variable_mapping import mapping, var_conv_oldDl1, track_mapping, dtype_list
from MV2_defaults import default_values2, track_defaults
import argparse
import datetime as dt

debug = True
tree_name = 'bTag_AntiKt4EMPFlowJets'#'bTag_AntiKt4EMTopoJets'
baseline_jet_cuts = 'jet_pt>20e3 & abs(jet_eta)<2.5 & (abs(jet_eta)>2.4 |\
                     jet_pt>60e3 | jet_JVT>0.5) & (jet_aliveAfterOR ==True)' # Set JVT to 0.5 to match training-dataset-dumper and recommendations https://twiki.cern.ch/twiki/bin/viewauth/AtlasProtected/PileupJetRecommendations
trk_pt_min = 1000.0
trk_eta_max = 2.5
trk_d0_max = 1.0
trk_z0_max = 1.5

trk_nPixHits_min = 1
trk_nSiHits_min = 7
trk_nSiSharedHits_max = 1
trk_nSiHoles_max = 2
trk_nPixHoles_max = 1

n_tracks = 40
n_evts_per_file = 25000
sort_var = 'IP3D_signed_d0_significance'

def GetArgs():

    """parse arguments"""

    parser = argparse.ArgumentParser(
        description="ROOT to hdf5 converter",
        usage="python convert_fromROOT.py <options>"
        )

    required = parser.add_argument_group("required arguments")

    required.add_argument(
        "--input", action="store", dest="input_files",
        metavar="input files", required=True, nargs='+',
        help="full path and filename to input ROOT files to merge, using bash wildcards to select multiple files"
    )

    required.add_argument(
        "--output", action="store", dest="output",
        metavar="[path/to/filename]", required=True,
        help="path to output hdf5 file. Do not include a trailing slash --> /"
        )

    optional = parser.add_argument_group("optional arguments")

    optional.add_argument(
        "--events", action="store", dest="events", type=int,
        required=False, default=1e6, help="Amount of events to process"
        )

    optional.add_argument(
        "--single-file", action="store_true", dest="single",
        help="Option to save all events in one file"
        )
    optional.add_argument(
        "--track_type", action="store", dest="track_type", type=str,
        required=False, default="nom", help="Type of track to use"
    )
    optional.add_argument(
        '--write_tracks', action='store_true', dest='write_tracks', default=False,
        help="Write track information to output file"
    )
    split = parser.add_mutually_exclusive_group()
    split.add_argument('--even', action='store_true')
    split.add_argument('--odd', action='store_true')
    category = parser.add_mutually_exclusive_group()
    category.add_argument('--bjets', action='store_true')
    category.add_argument('--cjets', action='store_true')
    category.add_argument('--ujets', action='store_true')
    return parser.parse_args()


def FindCheck(jetVec):
    default_location = np.argwhere(np.isnan(jetVec))
    jet_feature_check = np.zeros(len(jetVec))
    jet_feature_check[default_location] = 1
    return jet_feature_check

def get_bH_pt(row):
    # https://stackoverflow.com/questions/26658240/getting-the-index-of-a-row-in-a-pandas-apply-function
    return row['jet_bH_pt'][row.name[1]]

def get_file_suffix(filename):
    # Get a meaningful suffix for output filename from input filename
    return '/'.join(filename.split('/')[-2:]).replace('.root','').replace('flav_Akt4EMPf','').replace('trk_','').strip('/').split('._')[-1]

def GetTree(file_name, add_cuts, estart, estop, write_tracks = False):
    """Retrieves the events in the TTree with uproot and returns them as
    a pandas DataFrame."""
    if debug:
        t0_jets = dt.datetime.now()
        print('Start GetTree (entrystart = {},entrystop = {})'.format(estart, estop))
    var_list = list(mapping.keys())
    if write_tracks:
        track_var_list = list(track_mapping.keys())+['jet_phi_orig']
        # Not currently in FTAG framework ntuples
        track_var_list.remove('jet_trk_ip2d_grade')
        var_list += track_var_list
    tree = up.open(file_name)[tree_name]
    df = tree.pandas.df(var_list,entrystart=estart,entrystop=estop)
    if debug:
        print('Getting df with uproot took: {}'.format(dt.datetime.now()-t0_jets))
        t0_jets = dt.datetime.now()
    # Get b-hadron pT
    df['jet_bH_pt'] = df.apply(get_bH_pt, axis=1)
    # Now get max
    df['jet_bH_pt'] = df['jet_bH_pt'].apply(max)
    # Now set negative values to 0
    df['jet_bH_pt'] = df['jet_bH_pt'].mask(df['jet_bH_pt'].lt(0),0)

    # Add jet pT rank
    df['jetPtRank'] = df.groupby(level=0)['jet_pt'].rank(ascending=False)

    # If jet_jf_dR is larger than 15, it was set to the "default" value of std::hypot(-11,-11), so set this to its actual default of -1
    df['jet_jf_dR'] = df['jet_jf_dR'].mask(df['jet_jf_dR'].gt(15), default_values2['jf_dR'][0])

    # Apply jet quality cuts
    df.query(baseline_jet_cuts, inplace=True)

    if add_cuts != "":
        df.query(add_cuts, inplace=True)
    
    if debug:
        print('Querying jets df took: {}'.format(dt.datetime.now()-t0_jets))
        t0_jets = dt.datetime.now()
    if write_tracks:
        tracks_ndarray = GetTracks(df)
    if debug:
        print('Getting tracks ndarray took a total of: {}'.format(dt.datetime.now()-t0_jets))
        t0_jets = dt.datetime.now()

    df.rename(index=str, columns=mapping, inplace=True)

    # changing eta to absolute eta
    df['absEta_btagJes'] = df['eta_btagJes'].abs()
    # Replacing default values with this synthax
    # df.replace({'A': {0: 100, 4: 400}})
    rep_dict = {}
    for key, val in default_values2.items():
        if key in list(var_conv_oldDl1.keys()):
            replacer = {}
            for elem in val:
                replacer[elem] = np.nan
            rep_dict[var_conv_oldDl1[key]] = replacer
    df.replace(rep_dict, inplace=True)

    # Generating default flags
    df['JetFitter_isDefaults'] = FindCheck(df['JetFitter_mass'].values)
    df['SV1_isDefaults'] = FindCheck(df['SV1_masssvx'].values)
    df['IP2D_isDefaults'] = FindCheck(df['IP2D_bu'].values)
    df['IP3D_isDefaults'] = FindCheck(df['IP3D_bu'].values)
    df['JetFitterSecondaryVertex_isDefaults'] = FindCheck(df['JetFitterSecondaryVertex_nTracks'].values)
    # rnnip default flag not necessary anymore
    df['rnnip_isDefaults'] = FindCheck(df['rnnip_pu'].values)

    if debug:
        print('Remaining jets columns took: {}'.format(dt.datetime.now()-t0_jets))
        t0_jets = dt.datetime.now()

    if write_tracks:
        # Drop unnecessary columns
        df.drop(columns=track_var_list, inplace=True)    
        return df, tracks_ndarray
    else:
        return df

def get_sort_index(arr, sort_type=None):
    '''
    Taken from: https://gitlab.cern.ch/atlas-flavor-tagging-tools/rnnip/-/blob/master/root_to_np.py
    Find the indices that sort an array

    Inputs:
    - arr: A 1d np.array that we're sorting by
    - sort_type: A string indicating what type of variables that we're sorting by
        - If 'abs' is in sort_type, it will use the abs of arr for the sort
        - If 'neg' is in sort_type, it will multiply the array by -1 for the sort
        - If 'rev' is in the string, the sort will be in descending order.

    Output:
    - indices: An np.array of ints of the same shape as arr

    '''

    if 'abs' in sort_type:
        indices = np.argsort( np.abs(arr) )
    elif 'neg' in sort_type:
        raise NotImplementedError
        #indices = np.argsort(-arr)
    else:
        indices = np.argsort(arr)

    if 'rev' in sort_type:
        return indices[::-1]
    else:
        return indices
    
def getdPhi(jphi, tphi):
    # Calculate delta phi, accounting the for the 2pi at the boundary
    dphi = tphi - jphi
    dphi[dphi >  np.pi] = dphi[dphi >  np.pi] - 2*np.pi
    dphi[dphi < -np.pi] = dphi[dphi < -np.pi] + 2*np.pi
    return dphi
    
def getdR(jeta, jphi, teta, tphi):
    '''
    Taken from: https://gitlab.cern.ch/atlas-flavor-tagging-tools/rnnip/-/blob/master/root_to_np.py
    Calculate dR, accounting for the 2pi difference b/w for phi b/w pi and -pi.

    Inputs:
    - jeta, jphi: (floats) for the jet axis direction
    - teta, tphi: (np.arrays) for the track dir

    Returns:
    - tdrs: An np array for the opening angles b/w the tracks and the jet axis.

    '''

    deta = teta - jeta
    dphi = getdPhi(jphi, tphi)
    tdrs = np.sqrt( deta**2 + dphi**2)
    
    return tdrs

def get_trk_mask(row):
    mask = (row['jet_trk_pt'] > trk_pt_min) & \
           (np.abs(row['jet_trk_d0']) < trk_d0_max) & \
           (np.abs(row['jet_trk_z0']) < trk_z0_max) & \
           (np.abs(row['jet_trk_eta']) < trk_eta_max) & \
           (row['jet_trk_nPixHits'] >= trk_nPixHits_min) & \
           ((row['jet_trk_nPixHits'] + row['jet_trk_nSCTHits']) >= trk_nSiHits_min) & \
           ((row['jet_trk_nsharedPixHits'] + np.floor(row['jet_trk_nsharedSCTHits']/2))<= trk_nSiSharedHits_max) & \
           ((row['jet_trk_nPixHoles'] + row['jet_trk_nSCTHoles']) <= trk_nSiHoles_max) & \
           (row['jet_trk_nPixHoles'] <= trk_nPixHoles_max)
   
    return mask

def GetTracks(df):
    """Retrieves track information from pandas DataFrame
    obtained from events in TTree with uproot and returns
    them as a numpy ndarray."""
    
    if debug:
        t0 = dt.datetime.now()

    # Don't want to modify the jets df here
    df = df.copy(deep=True)

    if debug:
        print('\tDeep copy took: {}'.format(dt.datetime.now()-t0))
        t0 = dt.datetime.now()

    # Unroll track quantities per jet
    for col_name in track_mapping.keys():
        if col_name == 'jet_trk_ip2d_grade':
            continue
        # Unpack lists
        df[col_name] = df.apply(lambda row: row[col_name][row.name[1]], axis=1)
        # Convert lists to numpy arrays
        df[col_name] = df.apply(lambda row: np.array(row[col_name]), axis=1)
    
    if debug:
        print('\tUnrolling tracks took: {}'.format(dt.datetime.now()-t0))
        t0 = dt.datetime.now()

    # Update old IP3D default (-10) to new default
    df['jet_trk_ip3d_grade'] = df.apply(lambda row: np.where(row['jet_trk_ip3d_grade']==-10, track_defaults['IP3D_grade'], row['jet_trk_ip3d_grade']), axis=1)
    # Add jet_trk_ip2d_grade as IP3D equivalent
    df['jet_trk_ip2d_grade'] = df['jet_trk_ip3d_grade']

    if debug:
        print('\tUpdating IPxD grades took: {}'.format(dt.datetime.now()-t0))
        t0 = dt.datetime.now()

    # Fix z0 --> z0sintheta
    df['jet_trk_z0'] = df.apply(lambda row: row['jet_trk_z0']*np.sin(row['jet_trk_theta']), axis=1)

    # Calculate the derived variables: deta/dphi/dr/ptfrac
    df['deta'] = df['jet_trk_eta'] - df['jet_eta_orig']
    df['dphi'] = df.apply(lambda row: getdPhi(row['jet_phi_orig'], row['jet_trk_phi']), axis=1)
    df['dr'] = df.apply(lambda row: getdR(row['jet_eta_orig'], row['jet_phi_orig'], row['jet_trk_eta'], row['jet_trk_phi']), axis=1)
    df['ptfrac'] = df['jet_trk_pt']/df['jet_pt_orig']

    if debug:
        print('\tDerived variables calculation took: {}'.format(dt.datetime.now()-t0))
        t0 = dt.datetime.now()

    # Apply track selection
    all_track_vars_orig = list(track_mapping.keys()) + ['deta','dphi','dr','ptfrac']
    # First get indices to delete
    df['trk_mask'] = df.apply(lambda row: get_trk_mask(row), axis=1)

    if debug:
        print('\tGetting track mask took: {}'.format(dt.datetime.now()-t0))
        t0 = dt.datetime.now()

    # Now apply these to all columns
    for col_name in all_track_vars_orig:
        df[col_name] = df.apply(lambda row: row[col_name][row['trk_mask']], axis=1)    

    if debug:
        print('\tApplying track selection took: {}'.format(dt.datetime.now()-t0))
        t0 = dt.datetime.now()

    # Rename columns
    df.rename(index=str, columns=track_mapping, inplace=True)

    if debug:
        print('\tRenaming columns took: {}'.format(dt.datetime.now()-t0))
        t0 = dt.datetime.now()
    
    # Sort the track variable arrays according to sort_var
    all_track_vars = list(track_mapping.values()) + ['deta','dphi','dr','ptfrac']

    # Add column of indices (default sort in descending order)
    df['sort_indices'] = df.apply(lambda row: get_sort_index(row[sort_var],'rev'), axis=1)

    if debug:
        print('\tGetting track sorting indices took: {}'.format(dt.datetime.now()-t0))
        t0 = dt.datetime.now()

    # Apply sorting and pad to n_tracks 
    for col_name in all_track_vars:
        # # Apply sorting to track variable columns
        df[col_name] = df.apply(lambda row: row[col_name][row['sort_indices']], axis=1)

    if debug:
        print('\tApplying sorting took: {}'.format(dt.datetime.now()-t0))
        t0 = dt.datetime.now()

    for col_name in all_track_vars:
        # Pad track variable columns with zeros up to n_tracks (using nans gives errors)
        df[col_name] = df.apply(lambda row: np.pad(row[col_name][:n_tracks], (0, max(0, n_tracks-len(row[col_name]))), 'constant', constant_values=track_defaults[col_name]), axis=1)

    if debug:
        print('\tPadding took: {}'.format(dt.datetime.now()-t0))
        t0 = dt.datetime.now()

    # Zip track variables together into tuples
    df['tuple'] = df.apply(lambda row: list(zip( *[row[var] for var in all_track_vars] )), axis=1)

    if debug:
        print('\tZipping variables to tuples took: {}'.format(dt.datetime.now()-t0))
        t0 = dt.datetime.now()

    # Save tuples as ndarray
    tracks_ndarray = np.array(list(df['tuple']),dtype=np.dtype(dtype_list))

    if debug:
        print('\tDumping zipped variables to ndarray took: {}'.format(dt.datetime.now()-t0))
        t0 = dt.datetime.now()

    return tracks_ndarray


def __run():
    args = GetArgs()
    t0_glob = dt.datetime.now()
    if debug:
        print('Arguments:')
        for arg in vars(args): print(arg, getattr(args, arg))
        print('')
        
    events = 0
    df_out = None
    # additional cuts on eventnb and class label
    add_cuts = ""
    parity_cut = ""
    if args.even:
        add_cuts = "(eventnb % 2 == 0)"
    elif args.odd:
        add_cuts = "(eventnb % 2 == 1)"
    if args.bjets:
        parity_cut = "(jet_LabDr_HadF == 5)"
    elif args.cjets:
        parity_cut = "(jet_LabDr_HadF == 4)"
    elif args.ujets:
        parity_cut = "(jet_LabDr_HadF == 0)"
    if parity_cut != "":
        add_cuts += "& %s" % parity_cut

    for i, file in enumerate(args.input_files):
        sample_type = 'zp'
        if '427081' in file: sample_type = 'zp_extended'
        if '410470' in file: sample_type = 'ttbar'

        events_rest = int(args.events - events)
        if events_rest <= 0:
            print('Processed {} events; breaking!'.format(events))
            break
        # print(events_rest, "events more to process")
        sys.stdout.write('\r')
        # the exact output you're looking for:
        j = (events + 1) / args.events
        sys.stdout.write("%i/%i  [%-20s] %d%%" % (events, args.events,
                                                  '='*int(20*j), 100*j))
        sys.stdout.flush()

        nevts = up.numentries(file, tree_name)
        if debug:
            print('\nFile has {} events'.format(nevts))
        start = 0
        step = n_evts_per_file
        sub_jobs = [[start, step]]
        if n_evts_per_file > 0:
            nevts -= step
            while nevts > 0:
                start += step
                stop = start + n_evts_per_file 
                sub_jobs.append([start, stop])
                nevts -= n_evts_per_file
        if debug:
            print('Will run {} sub-jobs with {} events:'.format(len(sub_jobs), n_evts_per_file))
            for sj in sub_jobs: print('\t{}'.format(sj))

        # Split into smaller jobs to avoid memory issues
        for i_sub, subjob in enumerate(sub_jobs):
            estart = subjob[0]
            estop = subjob[1]
            if args.write_tracks:
                df, tracks_ndarray = GetTree(file, add_cuts, estart, estop, write_tracks=True)
            else:
                df = GetTree(file, add_cuts, estart, estop)

            if args.single is False:
                suffix = get_file_suffix(file)
                outfile_name = '{}/{}_{}-{}_sub{}.h5'.format(args.output,sample_type,args.track_type,suffix,i_sub)
                print('Saving to output file: {}'.format(outfile_name))
                h5f = h5py.File(outfile_name, 'w')
                h5f.create_dataset('jets',
                                data=df.to_records(index=False)[:],compression='gzip')
                if args.write_tracks:
                    h5f.create_dataset('tracks',
                                    data=tracks_ndarray,compression='gzip')
                h5f.close()
            else:
                if df_out is None:
                    df_out = df
                else:
                    df_out = pd.concat([df_out, df])
        events += len(df.groupby(level=0))
    print("")
    print('args.events: {}'.format(args.events))
    if args.single:
        outfile_name = '{}/{}_{}-merged.h5'.format(args.output,sample_type,args.track_type)
        print('Saving to output file: {}'.format(outfile_name))
        h5f = h5py.File(outfile_name, 'w')
        h5f.create_dataset('jets', data=df_out.sample(frac=1).to_records(
            index=False)[:int(args.events)],compression='gzip')
        h5f.close()
    print('Entire conversion process took: {}'.format(dt.datetime.now()-t0_glob))    
    print('Conversion successful!')

__run()
