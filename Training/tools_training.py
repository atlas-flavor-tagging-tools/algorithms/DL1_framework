from keras.callbacks import Callback
import h5py
import matplotlib
matplotlib.use('agg')
import matplotlib.pyplot as plt
import keras.backend as K
from keras.layers import Layer
from keras.layers import initializers, InputSpec
import json


class Swish(Layer):
    """
    Swish activation function with a trainable parameter referred to as 'beta'
    in https://arxiv.org/abs/1710.05941"""
    def __init__(self, trainable_beta=True, beta_initializer='ones', **kwargs):
        super(Swish, self).__init__(**kwargs)
        self.supports_masking = True
        self.trainable = trainable_beta
        self.beta_initializer = initializers.get(beta_initializer)
        self.__name__ = 'swish'

    def build(self, input_shape):
        self.beta = self.add_weight(shape=[1], name='beta',
                                    initializer=self.beta_initializer)
        self.input_spec = InputSpec(ndim=len(input_shape))
        self.built = True

    def call(self, inputs):
        return inputs * K.sigmoid(self.beta * inputs)

    def get_config(self):
        config = {'trainable_beta': self.trainable,
                  'beta_initializer': initializers.serialize(self.beta_initializer)}
        base_config = super(Swish, self).get_config()
        return dict(list(base_config.items()) + list(config.items()))


class MyCallback(Callback):
    def __init__(self, test_data=0, test_label=0, log_file=None, verbose=False,
                 model_name='test'):
        self.test_data = test_data
        self.test_label = test_label
        self.result = []
        self.log = open(log_file, 'w') if log_file else None
        self.verbose = verbose
        self.model_name = model_name

    def on_epoch_end(self, epoch, logs=None):
        # probs = self.model.predict(self.test_data)
        self.model.save('%s/model_epoch%i.h5' % (self.model_name,
                                                         epoch))
        print('accuracy:', logs['acc'])
        loss_list = [logs['loss']]
        acc_list = [logs['acc']]
        eval_loss = self.model.evaluate(self.test_data, self.test_label,
                                        batch_size=300, verbose=0)

        loss_file = "%s/LossFile_Epoch%i.h5" % (self.model_name, epoch)
        h5f = h5py.File(loss_file, 'w')
        h5f.create_dataset('train_loss_list', data=loss_list)
        h5f.create_dataset('eval_loss_list', data=eval_loss)
        h5f.create_dataset('train_acc_list', data=acc_list)
        h5f.close()
        # function to calculate acc
        # avg_acc = calculate_acc(probs, self.test_label)
        # self.result.append(avg_acc)
        # log_str = 'Epoch {} acc= {:.4f}'.format(epoch, avg_acc)
        # if self.verbose:
        #     print(log_str)
        # if self.log:
        #     print(log_str, file=self.log)

    def on_train_end(self, logs=None):
        if self.log:
            self.log.close()


class MyCallback_Aux(Callback):
    def __init__(self, test_data=0, test_label=0, log_file=None, verbose=False,
                 model_name='test'):
        self.test_data = test_data
        self.test_label = test_label
        self.result = []
        self.log = open(log_file, 'w') if log_file else None
        self.verbose = verbose
        self.model_name = model_name

    def on_epoch_end(self, epoch, logs=None):
        # probs = self.model.predict(self.test_data)
        self.model.save('%s/model_epoch%i.h5' % (self.model_name,
                                                         epoch))
        # print('accuracy:', logs['acc'])
        # loss_list = [logs['loss']]
        # acc_list = [logs['acc']]
        dict_epoch = {
            "loss": logs['loss'],
            "category_output_loss": logs['category_output_loss'],
            "file_output_loss": logs['file_output_loss'],
            "category_output_acc": logs['category_output_acc'],
            "file_output_acc": logs['file_output_acc'],
            "val_loss": logs['val_loss'],
            "val_category_output_loss": logs['val_category_output_loss'],
            "val_file_output_loss": logs['val_file_output_loss'],
            "val_category_output_acc": logs['val_category_output_acc'],
            "val_file_output_acc": logs['val_file_output_acc']
        }
        eval_loss = self.model.evaluate(self.test_data, self.test_label,
                                        batch_size=300, verbose=0)
        with open('%s/DictFile_Epoch%i.json' % (self.model_name,
                                                epoch), 'w') as outfile:
            json.dump(dict_epoch, outfile, indent=4)
        loss_file = "%s/LossFile_Epoch%i.h5" % (self.model_name, epoch)
        h5f = h5py.File(loss_file, 'w')
        # h5f.create_dataset('dict_epoch', data=dict_epoch)
        h5f.create_dataset('eval_loss_list', data=eval_loss)
        # h5f.create_dataset('train_acc_list', data=acc_list)
        h5f.close()
        # function to calculate acc
        # avg_acc = calculate_acc(probs, self.test_label)
        # self.result.append(avg_acc)
        # log_str = 'Epoch {} acc= {:.4f}'.format(epoch, avg_acc)
        # if self.verbose:
        #     print(log_str)
        # if self.log:
        #     print(log_str, file=self.log)

    def on_train_end(self, logs=None):
        if self.log:
            self.log.close()


class LRFinder(Callback):
    '''
    A simple callback for finding the optimal learning rate range for your
    model + dataset.

    # Usage
    ```python
        lr_finder = LRFinder(min_lr=1e-5,
                             max_lr=1e-2,
                             steps_per_epoch=np.ceil(epoch_size/batch_size),
                             epochs=3)
        model.fit(X_train, Y_train, callbacks=[lr_finder])

        lr_finder.plot_loss()
    ```

    # Arguments
        min_lr: The lower bound of the learning rate range for the experiment.
        max_lr: The upper bound of the learning rate range for the experiment.
        steps_per_epoch: Number of mini-batches in the dataset. Calculated as
                        `np.ceil(epoch_size/batch_size)`.
        epochs: Number of epochs to run experiment. Usually between 2 and 4
                epochs is sufficient.

    # References
        Blog post: jeremyjordan.me/nn-learning-rate
        Original paper: https://arxiv.org/abs/1506.01186
    '''

    def __init__(self, min_lr=1e-5, max_lr=1e-2, steps_per_epoch=None,
                 epochs=None):
        super().__init__()

        self.min_lr = min_lr
        self.max_lr = max_lr
        self.total_iterations = steps_per_epoch * epochs
        self.iteration = 0
        self.history = {}

    def clr(self):
        '''Calculate the learning rate.'''
        x = self.iteration / self.total_iterations
        return self.min_lr + (self.max_lr - self.min_lr) * x

    def on_train_begin(self, logs=None):
        '''Initialize the learning rate to the minimum value at the start of
            training.'''
        logs = logs or {}
        K.set_value(self.model.optimizer.lr, self.min_lr)

    def on_batch_end(self, epoch, logs=None):
        '''Record previous batch statistics and update the learning rate.'''
        logs = logs or {}
        self.iteration += 1

        self.history.setdefault('lr', []).append(K.get_value(self.model.optimizer.lr))
        self.history.setdefault('iterations', []).append(self.iteration)

        for k, v in logs.items():
            self.history.setdefault(k, []).append(v)

        K.set_value(self.model.optimizer.lr, self.clr())

    def plot_lr(self):
        '''Helper function to quickly inspect the learning rate schedule.'''
        plt.plot(self.history['iterations'], self.history['lr'])
        plt.yscale('log')
        plt.xlabel('Iteration')
        plt.ylabel('Learning rate')
        plt.show()
        plt.savefig("Plot_lr.pdf", transparent=True)

    def plot_loss(self):
        '''Helper function to quickly observe the learning rate experiment
            results.'''
        plt.plot(self.history['lr'], self.history['loss'])
        plt.xscale('log')
        plt.xlabel('Learning rate')
        plt.ylabel('Loss')
        plt.show()
        plt.savefig("Plot_lr_loss.pdf", transparent=True)
