"""
 ******************************************************************************
 * Project: DL1_framework                                                     *
 * Package: atlas-flavor-tagging-tools                                        *
 *                                                                            *
 * Description:                                                               *
 *      Training script for DL1 tagger                                        *
 *                                                                            *
 * Authors:                                                                   *
 *      ATLAS flavour tagging group, CERN, Geneva                             *
 *                                                                            *
 ******************************************************************************
"""
from __future__ import print_function
import h5py
import numpy as np
import os
os.environ['KERAS_BACKEND'] = 'tensorflow'
# os.environ['PYTHONHASHSEED'] = '0'
# os.environ['OMP_NUM_THREADS'] = '1'

import argparse

# import necessary keras modules for training
from keras.utils import np_utils
from keras.models import Sequential
from keras.layers import Dense, Dropout, Activation
from keras.optimizers import Adam
from keras.layers.normalization import BatchNormalization

from hyperopt import Trials, STATUS_OK, tpe
from hyperas import optim
from hyperas.distributions import choice, uniform


# def get_args():
#     """Define arg parse arguments."""
#     parser = argparse.ArgumentParser(description=' Options for keras training')
#     parser.add_argument('-t', '--training_file', type=str,
#                         default='',
#                         help='Enter the name of preprocessed training file',
#                         required=True)
#     parser.add_argument('-v', '--validation_file', type=str,
#                         default='',
#                         help='Enter the name of preprocessed validation file',
#                         required=True)
#     parser.add_argument('-m', '--model_name', type=str,
#                         default='',
#                         help='''Enter Model name. Will create new folder in
#                         results where output files are stored. If not
#                         specified, results are directly stored in results
#                         folder.''')
#     return parser.parse_args()
#
#
# args = get_args()
# if not os.path.exists('results'):
#     os.makedirs('results')
# if args.model_name != '':
#     if not os.path.exists('results/%s' % args.model_name):
#         os.makedirs('results/%s' % args.model_name)


def data():
    """Retrieves the data and returns data to pass to the model."""
    training_file = '/data/mg294/temp_tuples/hybrid_training_sample_MC16D_reduced_even_renamed_preprocessed_ordered.h5'
    test_file = '/data/mg294/temp_tuples/DL1validation_ttbar_MC16D_reduced_odd_renamed_preprocessed.h5'
    training_file = '/home/manuel/workspace/DL1_framework/samples/hybrid_training_sample_MC16D_reduced_even_renamed_preprocessed_ordered.h5'
    test_file = '/home/manuel/workspace/DL1_framework/samples/DL1validation_ttbar_MC16D_reduced_odd_renamed_preprocessed.h5'
    testinf = h5py.File(training_file, 'r')
    test_file = h5py.File(test_file, 'r')
    np.random.seed(42)  # for reproducibility

    train_bjet = testinf['train_processed_bjets'][:]
    train_cjet = testinf['train_processed_cjets'][:]
    train_ujet = testinf['train_processed_ujets'][:]
    b_weight = train_bjet['weight']
    c_weight = train_cjet['weight']
    u_weight = train_ujet['weight']

    test_bjet = test_file['train_processed_bjets'][:]
    test_cjet = test_file['train_processed_cjets'][:]
    test_ujet = test_file['train_processed_ujets'][:]

    var_names = list(train_bjet.dtype.names)
    var_names.remove('label')
    var_names.remove('weight')
    X_train = np.concatenate((train_ujet, train_cjet, train_bjet))
    X_test = np.concatenate((test_ujet, test_cjet, test_bjet))
    y_train = np.concatenate((np.zeros(len(train_ujet)),
                              np.ones(len(train_cjet)),
                              2 * np.ones(len(train_bjet))))
    y_test = np.concatenate((np.zeros(len(test_ujet)),
                             np.ones(len(test_cjet)),
                             2 * np.ones(len(test_bjet))))
    Y_train = np_utils.to_categorical(y_train, 3)
    Y_test = np_utils.to_categorical(y_test, 3)

    X_train = X_train[var_names]
    X_train = np.array(X_train.tolist())
    X_test = X_test[var_names]
    X_test = np.array(X_test.tolist())
    X_weights = np.concatenate((u_weight, c_weight, b_weight))

    rng_state = np.random.get_state()
    np.random.shuffle(X_train)
    np.random.set_state(rng_state)
    np.random.shuffle(Y_train)
    np.random.set_state(rng_state)
    np.random.shuffle(X_weights)

    print(np.shape(X_train))
    print(np.shape(Y_train))
    print(np.shape(X_weights))

    return X_train, Y_train, X_weights, X_test, Y_test
    # return X_train, Y_train, X_test, Y_test


def create_model(X_train, Y_train, X_weights, X_test, Y_test):
    """Keras model."""

    model = Sequential()

    model.add(Dense(units=72, input_shape=(46,),
                    activation='relu',
                    kernel_initializer='glorot_uniform'
                    ))
    #model.add(Activation('linear'))
    model.add(Dropout(0.1))
    model.add(BatchNormalization())
    # -------------------------- 2nd layer ------------------------- #
    model.add(Dense(units=57,
                    activation='relu',
                    kernel_initializer='glorot_uniform',
                    ))
    model.add(Dropout(0.2))
    model.add(BatchNormalization())
    # -------------------------- 3rd layer ------------------------- #
    model.add(Dense(units=60,
                    activation='relu',
                    kernel_initializer='glorot_uniform',
                    ))
    model.add(Dropout(0.2))
    model.add(BatchNormalization())
    # -------------------------- 4th layer ------------------------- #
    model.add(Dense(units=48,
                    activation='relu',
                    kernel_initializer='glorot_uniform',
                    ))
    model.add(Dropout(0.2))
    model.add(BatchNormalization())
    # -------------------------- 5th layer ------------------------- #
    model.add(Dense(units=36,
                    activation='relu',
                    kernel_initializer='glorot_uniform',
                    ))
    model.add(Dropout(0.2))
    model.add(BatchNormalization())
    # -------------------------- 6th layer ------------------------- #
    model.add(Dense(units=24,
                    activation='relu',
                    kernel_initializer='glorot_uniform'
                    ))
    model.add(Dropout(0.2))
    model.add(BatchNormalization())
    # -------------------------- 7th layer ------------------------- #
    model.add(Dense(units=12,
                    activation='relu',
                    kernel_initializer='glorot_uniform',
                    ))
    model.add(Dropout(0.2))
    model.add(BatchNormalization())
    # -------------------------- 8th layer ------------------------- #
    model.add(Dense(units=6,
                    activation='relu',
                    kernel_initializer='glorot_uniform',
                    ))
    model.add(Dropout(0.2))
    model.add(BatchNormalization())
    # -------------------------- 9th layer ------------------------- #
    model.add(Dense(3, input_shape=(6,),
              activation="softmax", kernel_initializer='glorot_uniform'))

    # model.summary()

    model_optimizer = Adam({{uniform(0.0001, 0.08)}})

    model.compile(  # loss='mse',
        loss='categorical_crossentropy',
        optimizer=model_optimizer,
        metrics=['accuracy'])

    hist_mod = model.fit(X_train, Y_train,
                         sample_weight=X_weights,
                         batch_size=300, epochs=2,
                         verbose=2,
                         validation_data=(X_test, Y_test)
                         )
    score, acc = model.evaluate(X_test, Y_test, verbose=0)
    print('Test accuracy:', acc)
    return {'loss': -acc, 'status': STATUS_OK, 'model': model}


if __name__ == '__main__':
    best_run, best_model = optim.minimize(model=create_model,
                                          data=data,
                                          algo=tpe.suggest,
                                          max_evals=2,
                                          trials=Trials())
    X_train, Y_train, X_weights, X_test, Y_test = data()
    print("Evalutation of best performing model:")
    print(best_model.evaluate(X_test, Y_test))
    print("Best performing model chosen hyper-parameters:")
    print(best_run)
# for epoch_i in range(1, 501):
#     print("start training epoch %i" % epoch_i)
    # model.save('results/%s/model_epoch%i.h5' % (args.model_name, epoch_i))
    # loss_list = hist_mod.history['loss']
    # acc_list = hist_mod.history['acc']
    # eval_loss = model.evaluate(X_train, Y_train, batch_size=300,
    #                            verbose=0)
    #
    # loss_file = "results/%s/LossFile_Epoch%i.h5" % (args.model_name,
    #                                                 epoch_i)
    # h5f = h5py.File(loss_file, 'w')
    # h5f.create_dataset('train_loss_list', data=loss_list)
    # h5f.create_dataset('eval_loss_list', data=eval_loss)
    # h5f.create_dataset('train_acc_list', data=acc_list)
    # h5f.close()

# model.save('results/%s/Final_model.h5' % args.model_name)


# # -------------------------- 1st layer ------------------------- #
# model.add(Dense(units=72, input_shape=(46,),
#                 kernel_initializer='glorot_uniform',
#                 activation={{choice(['relu', 'sigmoid'])}}
#                 # nb_feature=25
#                 ))
# #model.add(Activation('linear'))
# model.add({{choice([Dropout(0.1), Activation('linear')])}})
# model.add(Dropout({{uniform(0, 1)}}))
# model.add(BatchNormalization())
# # -------------------------- 2nd layer ------------------------- #
# model.add(Dense(units=57,
#                 activation={{choice(['relu', 'sigmoid'])}},
#                 kernel_initializer='glorot_uniform',
#                 ))
# model.add(Dropout({{uniform(0, 1)}}))
# model.add(BatchNormalization())
# # -------------------------- 3rd layer ------------------------- #
# model.add(Dense(units=60,
#                 activation={{choice(['relu', 'sigmoid'])}},
#                 kernel_initializer='glorot_uniform',
#                 ))
# model.add(Dropout({{uniform(0, 1)}}))
# model.add(BatchNormalization())
# # -------------------------- 4th layer ------------------------- #
# model.add(Dense(units=48,
#                 activation={{choice(['relu', 'sigmoid'])}},
#                 kernel_initializer='glorot_uniform',
#                 ))
# model.add(Dropout({{uniform(0, 1)}}))
# model.add(BatchNormalization())
# # -------------------------- 5th layer ------------------------- #
# model.add(Dense(units=36,
#                 activation={{choice(['relu', 'sigmoid'])}},
#                 kernel_initializer='glorot_uniform',
#                 ))
# model.add(Dropout({{uniform(0, 1)}}))
# model.add(BatchNormalization())
# # -------------------------- 6th layer ------------------------- #
# model.add(Dense(units=24,
#                       kernel_initializer='glorot_uniform',
#                       activation={{choice(['relu', 'sigmoid'])}}
#                       # nb_feature=25
#                       ))
# model.add(Dropout({{uniform(0, 1)}}))
# model.add(BatchNormalization())
# # -------------------------- 7th layer ------------------------- #
# model.add(Dense(units=12,
#                 activation={{choice(['relu', 'sigmoid'])}},
#                 kernel_initializer='glorot_uniform',
#                 ))
# model.add(Dropout({{uniform(0, 1)}}))
# model.add(BatchNormalization())
# # -------------------------- 8th layer ------------------------- #
# model.add(Dense(units=6,
#                 activation={{choice(['relu', 'sigmoid'])}},
#                 kernel_initializer='glorot_uniform',
#                 ))
# model.add(Dropout({{uniform(0, 1)}}))
# model.add(BatchNormalization())
# # -------------------------- 9th layer ------------------------- #
# model.add(Dense(3,
#           activation="softmax", kernel_initializer='glorot_uniform'))
#
# # model.summary()
#
# model_optimizer = Adam({{uniform(0.001, 0.08)}})
